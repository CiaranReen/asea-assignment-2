﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.Data.SqlClient;

namespace CalTrac
{
    public partial class Tracker : CalTrac.BaseForm
    {
        SqlCeConnection conn = new SqlCeConnection("Data Source=C:\\temp\\MyDatabase.sdf");

        public Tracker()
        {
            InitializeComponent();
            this.index();
        }

        public void index()
        {
            //Get each food group to populate the combo boxes
            string breakfastQuery = @"SELECT calories, name FROM tblFood WHERE [group] = 'breakfast'";
            string lunchQuery = @"SELECT calories, name FROM tblFood WHERE [group] = 'lunch'";
            string dinnerQuery = @"SELECT calories, name FROM tblFood WHERE [group] = 'dinner'";
            string snackQuery = @"SELECT calories, name FROM tblFood WHERE [group] = 'snack'";

            //Separate connection needed for each different food group
            using (SqlCeDataAdapter da = new SqlCeDataAdapter(breakfastQuery, conn))
            {
                conn.Open();
                DataSet ds = new DataSet();
                da.Fill(ds, "Food");
                breakfastComboBox.DisplayMember = "name";
                breakfastComboBox.ValueMember = "id";
                breakfastComboBox.DataSource = ds.Tables["Food"];
                conn.Close();
            }
            using (SqlCeDataAdapter da = new SqlCeDataAdapter(snackQuery, conn))
            {
                conn.Open();
                DataSet ds = new DataSet();
                da.Fill(ds, "Food");
                snack1ComboBox.DisplayMember = "name";
                snack1ComboBox.ValueMember = "id";
                snack1ComboBox.DataSource = ds.Tables["Food"];
                conn.Close();
            }
            using (SqlCeDataAdapter da = new SqlCeDataAdapter(lunchQuery, conn))
            {
                conn.Open();
                DataSet ds = new DataSet();
                da.Fill(ds, "Food");
                lunchComboBox.DisplayMember = "name";
                lunchComboBox.ValueMember = "id";
                lunchComboBox.DataSource = ds.Tables["Food"];
                conn.Close();
            }
            using (SqlCeDataAdapter da = new SqlCeDataAdapter(snackQuery, conn))
            {
                conn.Open();
                DataSet ds = new DataSet();
                da.Fill(ds, "Food");
                snack2ComboBox.DisplayMember = "name";
                snack2ComboBox.ValueMember = "id";
                snack2ComboBox.DataSource = ds.Tables["Food"];
                conn.Close();
            }
            using (SqlCeDataAdapter da = new SqlCeDataAdapter(dinnerQuery, conn))
            {
                conn.Open();
                DataSet ds = new DataSet();
                da.Fill(ds, "Food");
                dinnerComboBox.DisplayMember = "name";
                dinnerComboBox.ValueMember = "id";
                dinnerComboBox.DataSource = ds.Tables["Food"];
                conn.Close();
            }
            using (SqlCeDataAdapter da = new SqlCeDataAdapter(snackQuery, conn))
            {
                conn.Open();
                DataSet ds = new DataSet();
                da.Fill(ds, "Food");
                snack3ComboBox.DisplayMember = "name";
                snack3ComboBox.ValueMember = "id";
                snack3ComboBox.DataSource = ds.Tables["Food"];
                conn.Close();
            }
        }

        //Log the meal in Logged Days table
        private void logDayButton_Click(object sender, EventArgs e)
        {
            //Insert query
            string queryText = @"INSERT INTO tblLoggedDays (userId, breakfast, snack1, lunch, snack2, dinner, snack3, added) VALUES (@userId, @breakfast, @snack1, 
                                @lunch, @snack2, @dinner, @snack3, @added)";
            SqlCeCommand command = new SqlCeCommand(queryText, conn);

            //Added column needs to be set to the current date time
            DateTime myDateTime = DateTime.Now;
            string sqlFormattedDate = myDateTime.ToString("yyyy-MM-dd HH:mm:ss");

            //Bind the query values from the combo boxes
            command.Parameters.Add("@userId", 1);
            command.Parameters.Add("@breakfast", breakfastComboBox.Text);
            command.Parameters.Add("@snack1", snack1ComboBox.Text);
            command.Parameters.Add("@lunch", lunchComboBox.Text);
            command.Parameters.Add("@snack2", snack2ComboBox.Text);
            command.Parameters.Add("@dinner", dinnerComboBox.Text);
            command.Parameters.Add("@snack3", snack3ComboBox.Text);
            command.Parameters.Add("@added", sqlFormattedDate);

            conn.Open();
            command.ExecuteNonQuery();
            conn.Close();
            MessageBox.Show("Your day has been successfully logged!");
        }

        //Save in favourites table function
        private void favouriteButton_MouseClick(object sender, MouseEventArgs e)
        {
            //Insert query for favourites table
            string queryText = @"INSERT INTO tblFavourites (userId, breakfast, snack1, lunch, snack2, dinner, snack3) VALUES (@userId, @breakfast, @snack1, 
                                @lunch, @snack2, @dinner, @snack3)";
            SqlCeCommand command = new SqlCeCommand(queryText, conn);

            //Bind the values from the combo boxes
            command.Parameters.Add("@userId", 1);
            command.Parameters.Add("@breakfast", breakfastComboBox.Text);
            command.Parameters.Add("@snack1", snack1ComboBox.Text);
            command.Parameters.Add("@lunch", lunchComboBox.Text);
            command.Parameters.Add("@snack2", snack2ComboBox.Text);
            command.Parameters.Add("@dinner", dinnerComboBox.Text);
            command.Parameters.Add("@snack3", snack3ComboBox.Text);

            conn.Open();
            command.ExecuteNonQuery();
            conn.Close();
            MessageBox.Show("Saved in favourites!");
        }
    }
}