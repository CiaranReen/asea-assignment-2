﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalTrac
{
    public partial class BaseForm : Form
    {
        public BaseForm()
        {
            InitializeComponent();
        }

        private void overviewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Overview o = new Overview();
            this.Hide();
            o.ShowDialog();
        }

        private void trackerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tracker t = new Tracker();
            this.Hide();
            t.ShowDialog();
        }

        private void favouritesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Favourites f = new Favourites();
            this.Hide();
            f.ShowDialog();
        }
    }
}
