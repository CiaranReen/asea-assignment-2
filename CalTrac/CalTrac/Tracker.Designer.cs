﻿namespace CalTrac
{
    partial class Tracker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.logDayButton = new System.Windows.Forms.Button();
            this.breakfastComboBox = new System.Windows.Forms.ComboBox();
            this.snack1ComboBox = new System.Windows.Forms.ComboBox();
            this.lunchComboBox = new System.Windows.Forms.ComboBox();
            this.snack2ComboBox = new System.Windows.Forms.ComboBox();
            this.dinnerComboBox = new System.Windows.Forms.ComboBox();
            this.snack3ComboBox = new System.Windows.Forms.ComboBox();
            this.favouriteButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label1.Location = new System.Drawing.Point(12, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Enter Meal Details";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Breakfast";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Morning Snack";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Lunch";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 155);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Afternoon Snack";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 181);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Dinner";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 208);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Evening Snack";
            // 
            // logDayButton
            // 
            this.logDayButton.Location = new System.Drawing.Point(17, 248);
            this.logDayButton.Name = "logDayButton";
            this.logDayButton.Size = new System.Drawing.Size(112, 40);
            this.logDayButton.TabIndex = 8;
            this.logDayButton.Text = "Log this day!";
            this.logDayButton.UseVisualStyleBackColor = true;
            this.logDayButton.Click += new System.EventHandler(this.logDayButton_Click);
            // 
            // breakfastComboBox
            // 
            this.breakfastComboBox.FormattingEnabled = true;
            this.breakfastComboBox.Location = new System.Drawing.Point(139, 80);
            this.breakfastComboBox.Name = "breakfastComboBox";
            this.breakfastComboBox.Size = new System.Drawing.Size(156, 21);
            this.breakfastComboBox.TabIndex = 9;
            // 
            // snack1ComboBox
            // 
            this.snack1ComboBox.FormattingEnabled = true;
            this.snack1ComboBox.Location = new System.Drawing.Point(139, 107);
            this.snack1ComboBox.Name = "snack1ComboBox";
            this.snack1ComboBox.Size = new System.Drawing.Size(156, 21);
            this.snack1ComboBox.TabIndex = 10;
            // 
            // lunchComboBox
            // 
            this.lunchComboBox.FormattingEnabled = true;
            this.lunchComboBox.Location = new System.Drawing.Point(139, 129);
            this.lunchComboBox.Name = "lunchComboBox";
            this.lunchComboBox.Size = new System.Drawing.Size(156, 21);
            this.lunchComboBox.TabIndex = 11;
            // 
            // snack2ComboBox
            // 
            this.snack2ComboBox.FormattingEnabled = true;
            this.snack2ComboBox.Location = new System.Drawing.Point(139, 155);
            this.snack2ComboBox.Name = "snack2ComboBox";
            this.snack2ComboBox.Size = new System.Drawing.Size(156, 21);
            this.snack2ComboBox.TabIndex = 12;
            // 
            // dinnerComboBox
            // 
            this.dinnerComboBox.FormattingEnabled = true;
            this.dinnerComboBox.Location = new System.Drawing.Point(139, 182);
            this.dinnerComboBox.Name = "dinnerComboBox";
            this.dinnerComboBox.Size = new System.Drawing.Size(156, 21);
            this.dinnerComboBox.TabIndex = 13;
            // 
            // snack3ComboBox
            // 
            this.snack3ComboBox.FormattingEnabled = true;
            this.snack3ComboBox.Location = new System.Drawing.Point(139, 209);
            this.snack3ComboBox.Name = "snack3ComboBox";
            this.snack3ComboBox.Size = new System.Drawing.Size(156, 21);
            this.snack3ComboBox.TabIndex = 14;
            // 
            // favouriteButton
            // 
            this.favouriteButton.Location = new System.Drawing.Point(139, 248);
            this.favouriteButton.Name = "favouriteButton";
            this.favouriteButton.Size = new System.Drawing.Size(121, 40);
            this.favouriteButton.TabIndex = 15;
            this.favouriteButton.Text = "Favourite";
            this.favouriteButton.UseVisualStyleBackColor = true;
            this.favouriteButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.favouriteButton_MouseClick);
            // 
            // Tracker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(621, 414);
            this.Controls.Add(this.favouriteButton);
            this.Controls.Add(this.snack3ComboBox);
            this.Controls.Add(this.dinnerComboBox);
            this.Controls.Add(this.snack2ComboBox);
            this.Controls.Add(this.lunchComboBox);
            this.Controls.Add(this.snack1ComboBox);
            this.Controls.Add(this.breakfastComboBox);
            this.Controls.Add(this.logDayButton);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Tracker";
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.logDayButton, 0);
            this.Controls.SetChildIndex(this.breakfastComboBox, 0);
            this.Controls.SetChildIndex(this.snack1ComboBox, 0);
            this.Controls.SetChildIndex(this.lunchComboBox, 0);
            this.Controls.SetChildIndex(this.snack2ComboBox, 0);
            this.Controls.SetChildIndex(this.dinnerComboBox, 0);
            this.Controls.SetChildIndex(this.snack3ComboBox, 0);
            this.Controls.SetChildIndex(this.favouriteButton, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button logDayButton;
        private System.Windows.Forms.ComboBox breakfastComboBox;
        private System.Windows.Forms.ComboBox snack1ComboBox;
        private System.Windows.Forms.ComboBox lunchComboBox;
        private System.Windows.Forms.ComboBox snack2ComboBox;
        private System.Windows.Forms.ComboBox dinnerComboBox;
        private System.Windows.Forms.ComboBox snack3ComboBox;
        private System.Windows.Forms.Button favouriteButton;
    }
}