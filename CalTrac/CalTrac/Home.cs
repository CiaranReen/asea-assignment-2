﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.Data.SqlClient;

namespace CalTrac
{
    public partial class Home : Form
    {
        SqlCeConnection conn = new SqlCeConnection("Data Source=C:\\temp\\MyDatabase.sdf");

        public Home()
        {
            InitializeComponent();
        }

        //Login function
        private void loginButton_Click(object sender, EventArgs e)
        {
            //Check both text boxes have some input
            if (emailTextBox.Text != "" & passwordTextBox.Text != "")
            {
                //Try and find a match for the given credentials
                string queryText = @"SELECT Count(*) FROM tblUser 
                             WHERE email = @Email AND password = @Password";
                using (SqlCeCommand cmd = new SqlCeCommand(queryText, conn))
                {
                    conn.Open();

                    //Set the parameters with the credentials entered
                    cmd.Parameters.AddWithValue("@Email", emailTextBox.Text);
                    cmd.Parameters.AddWithValue("@Password", passwordTextBox.Text);

                    //If the credentials are valid then result will be '1'
                    int result = (int)cmd.ExecuteScalar();
                    if (result > 0)
                    {
                        conn.Close();
                        Overview o = new Overview();
                        this.Hide();
                        o.ShowDialog();
                    }
                    else
                    {
                        conn.Close();
                        MessageBox.Show("User Not Found!");
                    }
                }
            }
        }
    }
}
