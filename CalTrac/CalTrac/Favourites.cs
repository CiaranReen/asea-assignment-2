﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.Data.SqlClient;
using System.IO;

namespace CalTrac
{
    public partial class Favourites : CalTrac.BaseForm
    {
        SqlCeConnection conn = new SqlCeConnection("Data Source=C:\\temp\\MyDatabase.sdf");

        public Favourites()
        {
            InitializeComponent();
        }

        private void Favourites_Load(object sender, EventArgs e)
        {
            this.tblFavouritesTableAdapter.Fill(this.favouritesDataSet.tblFavourites);
        }

        //Update the database with the new values
        private void button1_Click(object sender, EventArgs e)
        {
                try
                {

                    SqlCeCommand cmd = new SqlCeCommand();
                    conn.Open();

                    DataTable dt = new DataTable();
                    SqlCeDataAdapter da = new SqlCeDataAdapter("SELECT * FROM tblFavourites WHERE id=" +
                    Convert.ToInt16(IDTextBox.Text) + " ", conn);
                    da.Fill(dt);

                    //Starts editing the selected row
                    dt.Rows[0].BeginEdit();

                    //Update all the rows 
                    dt.Rows[0][2] = breakfastTextBox.Text;
                    dt.Rows[0][3] = snack1TextBox.Text;
                    dt.Rows[0][4] = lunchTextBox.Text;
                    dt.Rows[0][5] = snack2TextBox.Text;
                    dt.Rows[0][6] = dinnerTextBox.Text;
                    dt.Rows[0][7] = snack3TextBox.Text;

                    //stop the editing
                    dt.Rows[0].EndEdit();

                    //declare sql command builder that allows the saving of records
                    SqlCeCommandBuilder cb = new SqlCeCommandBuilder(da);

                    //update the database
                    da.Update(dt);

                    //close the connection
                    conn.Close();

                    this.tblFavouritesTableAdapter.Fill(this.favouritesDataSet.tblFavourites);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }


            }

        //Populate the form with the selected row values
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            IDTextBox.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            string favouritesQuery = @"SELECT * FROM tblFavourites WHERE id = " + Convert.ToInt16(IDTextBox.Text);

            using (SqlCeDataAdapter da = new SqlCeDataAdapter(favouritesQuery, conn))
            {
                conn.Open();
                DataTable dt = new DataTable();
                da.Fill(dt);

                IDTextBox.Text = dt.Rows[0][0].ToString();
                breakfastTextBox.Text = dt.Rows[0][2].ToString();
                snack1TextBox.Text = dt.Rows[0][3].ToString();
                lunchTextBox.Text = dt.Rows[0][4].ToString();
                snack2TextBox.Text = dt.Rows[0][5].ToString();
                dinnerTextBox.Text = dt.Rows[0][6].ToString();
                snack3TextBox.Text = dt.Rows[0][7].ToString();

                conn.Close();
            }
        }

        //Delete method
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCeDataAdapter da = new SqlCeDataAdapter(" DELETE from tblFavourites where id=" + Convert.ToInt16(IDTextBox.Text) + "", conn);
                da.Fill(dt);

                SqlCeCommandBuilder cb = new SqlCeCommandBuilder(da);

                //Updates the database
                da.Update(dt);

                conn.Close();

                //call the method that displays records to the data gridview
                this.tblFavouritesTableAdapter.Fill(this.favouritesDataSet.tblFavourites);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }


        private void button3_Click(object sender, EventArgs e)
        {
            TextWriter sw = new StreamWriter(@"F:\\Year3ASDA\\favourites.txt");
            int rowcount = dataGridView1.Rows.Count;
            for (int i = 0; i < rowcount - 1; i++)
            {
                sw.WriteLine(dataGridView1.Rows[i].Cells[0].Value.ToString() + "\t" + dataGridView1.Rows[i].Cells[1].Value.ToString() + "\t" + dataGridView1.Rows[i].Cells[2].Value.ToString()
                    + "\t" + dataGridView1.Rows[i].Cells[3].Value.ToString() + "\t" + dataGridView1.Rows[i].Cells[4].Value.ToString() + "\t" + dataGridView1.Rows[i].Cells[5].Value.ToString()
                    + "\t" + dataGridView1.Rows[i].Cells[6].Value.ToString() + "\t" + dataGridView1.Rows[i].Cells[7].Value.ToString());
            }
            sw.Close();     //Don't Forget Close the TextWriter Object(sw)

            MessageBox.Show("Favourites Successfully Exported!");

        }
    }
}
