﻿namespace CalTrac
{
    partial class Favourites
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label4;
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.breakfastDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.snack1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lunchDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.snack2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dinnerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.snack3DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblFavouritesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.favouritesDataSet = new CalTrac.FavouritesDataSet();
            this.tblFavouritesTableAdapter = new CalTrac.FavouritesDataSetTableAdapters.tblFavouritesTableAdapter();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.IDTextBox = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.breakfastTextBox = new System.Windows.Forms.TextBox();
            this.snack1TextBox = new System.Windows.Forms.TextBox();
            this.lunchTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.snack2TextBox = new System.Windows.Forms.TextBox();
            this.dinnerTextBox = new System.Windows.Forms.TextBox();
            this.snack3TextBox = new System.Windows.Forms.TextBox();
            label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblFavouritesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.favouritesDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(17, 385);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(79, 13);
            label4.TabIndex = 10;
            label4.Text = "Morning Snack";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label1.Location = new System.Drawing.Point(12, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Favourites";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.userIdDataGridViewTextBoxColumn,
            this.breakfastDataGridViewTextBoxColumn,
            this.snack1DataGridViewTextBoxColumn,
            this.lunchDataGridViewTextBoxColumn,
            this.snack2DataGridViewTextBoxColumn,
            this.dinnerDataGridViewTextBoxColumn,
            this.snack3DataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.tblFavouritesBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(17, 93);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(844, 158);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // userIdDataGridViewTextBoxColumn
            // 
            this.userIdDataGridViewTextBoxColumn.DataPropertyName = "userId";
            this.userIdDataGridViewTextBoxColumn.HeaderText = "userId";
            this.userIdDataGridViewTextBoxColumn.Name = "userIdDataGridViewTextBoxColumn";
            // 
            // breakfastDataGridViewTextBoxColumn
            // 
            this.breakfastDataGridViewTextBoxColumn.DataPropertyName = "breakfast";
            this.breakfastDataGridViewTextBoxColumn.HeaderText = "breakfast";
            this.breakfastDataGridViewTextBoxColumn.Name = "breakfastDataGridViewTextBoxColumn";
            // 
            // snack1DataGridViewTextBoxColumn
            // 
            this.snack1DataGridViewTextBoxColumn.DataPropertyName = "snack1";
            this.snack1DataGridViewTextBoxColumn.HeaderText = "snack1";
            this.snack1DataGridViewTextBoxColumn.Name = "snack1DataGridViewTextBoxColumn";
            // 
            // lunchDataGridViewTextBoxColumn
            // 
            this.lunchDataGridViewTextBoxColumn.DataPropertyName = "lunch";
            this.lunchDataGridViewTextBoxColumn.HeaderText = "lunch";
            this.lunchDataGridViewTextBoxColumn.Name = "lunchDataGridViewTextBoxColumn";
            // 
            // snack2DataGridViewTextBoxColumn
            // 
            this.snack2DataGridViewTextBoxColumn.DataPropertyName = "snack2";
            this.snack2DataGridViewTextBoxColumn.HeaderText = "snack2";
            this.snack2DataGridViewTextBoxColumn.Name = "snack2DataGridViewTextBoxColumn";
            // 
            // dinnerDataGridViewTextBoxColumn
            // 
            this.dinnerDataGridViewTextBoxColumn.DataPropertyName = "dinner";
            this.dinnerDataGridViewTextBoxColumn.HeaderText = "dinner";
            this.dinnerDataGridViewTextBoxColumn.Name = "dinnerDataGridViewTextBoxColumn";
            // 
            // snack3DataGridViewTextBoxColumn
            // 
            this.snack3DataGridViewTextBoxColumn.DataPropertyName = "snack3";
            this.snack3DataGridViewTextBoxColumn.HeaderText = "snack3";
            this.snack3DataGridViewTextBoxColumn.Name = "snack3DataGridViewTextBoxColumn";
            // 
            // tblFavouritesBindingSource
            // 
            this.tblFavouritesBindingSource.DataMember = "tblFavourites";
            this.tblFavouritesBindingSource.DataSource = this.favouritesDataSet;
            // 
            // favouritesDataSet
            // 
            this.favouritesDataSet.DataSetName = "FavouritesDataSet";
            this.favouritesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tblFavouritesTableAdapter
            // 
            this.tblFavouritesTableAdapter.ClearBeforeFill = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(586, 318);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(126, 47);
            this.button1.TabIndex = 4;
            this.button1.Text = "Update Selected";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(735, 318);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(126, 47);
            this.button2.TabIndex = 5;
            this.button2.Text = "Delete Selected";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(586, 413);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(275, 27);
            this.button3.TabIndex = 6;
            this.button3.Text = "Export Favourites";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 318);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "ID";
            // 
            // IDTextBox
            // 
            this.IDTextBox.AutoSize = true;
            this.IDTextBox.Location = new System.Drawing.Point(42, 318);
            this.IDTextBox.Name = "IDTextBox";
            this.IDTextBox.Size = new System.Drawing.Size(0, 13);
            this.IDTextBox.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 352);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Breakfast";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 420);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Lunch";
            // 
            // breakfastTextBox
            // 
            this.breakfastTextBox.Location = new System.Drawing.Point(108, 352);
            this.breakfastTextBox.Name = "breakfastTextBox";
            this.breakfastTextBox.Size = new System.Drawing.Size(100, 20);
            this.breakfastTextBox.TabIndex = 12;
            // 
            // snack1TextBox
            // 
            this.snack1TextBox.Location = new System.Drawing.Point(108, 385);
            this.snack1TextBox.Name = "snack1TextBox";
            this.snack1TextBox.Size = new System.Drawing.Size(100, 20);
            this.snack1TextBox.TabIndex = 13;
            // 
            // lunchTextBox
            // 
            this.lunchTextBox.Location = new System.Drawing.Point(108, 420);
            this.lunchTextBox.Name = "lunchTextBox";
            this.lunchTextBox.Size = new System.Drawing.Size(100, 20);
            this.lunchTextBox.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(241, 352);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Afternoon Snack";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(241, 385);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Dinner";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(241, 423);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Evening Snack";
            // 
            // snack2TextBox
            // 
            this.snack2TextBox.Location = new System.Drawing.Point(345, 349);
            this.snack2TextBox.Name = "snack2TextBox";
            this.snack2TextBox.Size = new System.Drawing.Size(100, 20);
            this.snack2TextBox.TabIndex = 18;
            // 
            // dinnerTextBox
            // 
            this.dinnerTextBox.Location = new System.Drawing.Point(345, 382);
            this.dinnerTextBox.Name = "dinnerTextBox";
            this.dinnerTextBox.Size = new System.Drawing.Size(100, 20);
            this.dinnerTextBox.TabIndex = 19;
            // 
            // snack3TextBox
            // 
            this.snack3TextBox.Location = new System.Drawing.Point(345, 420);
            this.snack3TextBox.Name = "snack3TextBox";
            this.snack3TextBox.Size = new System.Drawing.Size(100, 20);
            this.snack3TextBox.TabIndex = 20;
            // 
            // Favourites
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(912, 544);
            this.Controls.Add(this.snack3TextBox);
            this.Controls.Add(this.dinnerTextBox);
            this.Controls.Add(this.snack2TextBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lunchTextBox);
            this.Controls.Add(this.snack1TextBox);
            this.Controls.Add(this.breakfastTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.IDTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label1);
            this.Name = "Favourites";
            this.Load += new System.EventHandler(this.Favourites_Load);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.dataGridView1, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.button2, 0);
            this.Controls.SetChildIndex(this.button3, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.IDTextBox, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(label4, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.breakfastTextBox, 0);
            this.Controls.SetChildIndex(this.snack1TextBox, 0);
            this.Controls.SetChildIndex(this.lunchTextBox, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.snack2TextBox, 0);
            this.Controls.SetChildIndex(this.dinnerTextBox, 0);
            this.Controls.SetChildIndex(this.snack3TextBox, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblFavouritesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.favouritesDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private FavouritesDataSet favouritesDataSet;
        private System.Windows.Forms.BindingSource tblFavouritesBindingSource;
        private FavouritesDataSetTableAdapters.tblFavouritesTableAdapter tblFavouritesTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn userIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn breakfastDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn snack1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lunchDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn snack2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dinnerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn snack3DataGridViewTextBoxColumn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label IDTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox breakfastTextBox;
        private System.Windows.Forms.TextBox snack1TextBox;
        private System.Windows.Forms.TextBox lunchTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox snack2TextBox;
        private System.Windows.Forms.TextBox dinnerTextBox;
        private System.Windows.Forms.TextBox snack3TextBox;
    }
}
