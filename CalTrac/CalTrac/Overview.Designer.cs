﻿namespace CalTrac
{
    partial class Overview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.label1 = new System.Windows.Forms.Label();
            this.mainChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tblLoggedDaysBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.myDatabaseDataSet = new CalTrac.MyDatabaseDataSet();
            this.day1Date = new System.Windows.Forms.Label();
            this.day2Date = new System.Windows.Forms.Label();
            this.day3Date = new System.Windows.Forms.Label();
            this.day5Date = new System.Windows.Forms.Label();
            this.day4Date = new System.Windows.Forms.Label();
            this.day7Date = new System.Windows.Forms.Label();
            this.day6Date = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.linkLabel4 = new System.Windows.Forms.LinkLabel();
            this.linkLabel5 = new System.Windows.Forms.LinkLabel();
            this.linkLabel6 = new System.Windows.Forms.LinkLabel();
            this.linkLabel7 = new System.Windows.Forms.LinkLabel();
            this.tblLoggedDaysTableAdapter = new CalTrac.MyDatabaseDataSetTableAdapters.tblLoggedDaysTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.mainChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblLoggedDaysBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.myDatabaseDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label1.Location = new System.Drawing.Point(12, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Week Overview";
            // 
            // mainChart
            // 
            chartArea1.Name = "ChartArea1";
            this.mainChart.ChartAreas.Add(chartArea1);
            this.mainChart.DataSource = this.tblLoggedDaysBindingSource;
            legend1.Name = "Legend1";
            this.mainChart.Legends.Add(legend1);
            this.mainChart.Location = new System.Drawing.Point(232, 65);
            this.mainChart.Name = "mainChart";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.mainChart.Series.Add(series1);
            this.mainChart.Size = new System.Drawing.Size(330, 306);
            this.mainChart.TabIndex = 2;
            this.mainChart.Text = "Weekly Calorie Count";
            // 
            // tblLoggedDaysBindingSource
            // 
            this.tblLoggedDaysBindingSource.DataMember = "tblLoggedDays";
            this.tblLoggedDaysBindingSource.DataSource = this.myDatabaseDataSet;
            // 
            // myDatabaseDataSet
            // 
            this.myDatabaseDataSet.DataSetName = "MyDatabaseDataSet";
            this.myDatabaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // day1Date
            // 
            this.day1Date.AutoSize = true;
            this.day1Date.Location = new System.Drawing.Point(17, 95);
            this.day1Date.Name = "day1Date";
            this.day1Date.Size = new System.Drawing.Size(35, 13);
            this.day1Date.TabIndex = 3;
            this.day1Date.Text = "label1";
            // 
            // day2Date
            // 
            this.day2Date.AutoSize = true;
            this.day2Date.Location = new System.Drawing.Point(17, 128);
            this.day2Date.Name = "day2Date";
            this.day2Date.Size = new System.Drawing.Size(35, 13);
            this.day2Date.TabIndex = 5;
            this.day2Date.Text = "label3";
            // 
            // day3Date
            // 
            this.day3Date.AutoSize = true;
            this.day3Date.Location = new System.Drawing.Point(17, 160);
            this.day3Date.Name = "day3Date";
            this.day3Date.Size = new System.Drawing.Size(35, 13);
            this.day3Date.TabIndex = 7;
            this.day3Date.Text = "label5";
            // 
            // day5Date
            // 
            this.day5Date.AutoSize = true;
            this.day5Date.Location = new System.Drawing.Point(17, 221);
            this.day5Date.Name = "day5Date";
            this.day5Date.Size = new System.Drawing.Size(35, 13);
            this.day5Date.TabIndex = 9;
            this.day5Date.Text = "label5";
            // 
            // day4Date
            // 
            this.day4Date.AutoSize = true;
            this.day4Date.Location = new System.Drawing.Point(17, 189);
            this.day4Date.Name = "day4Date";
            this.day4Date.Size = new System.Drawing.Size(35, 13);
            this.day4Date.TabIndex = 11;
            this.day4Date.Text = "label5";
            // 
            // day7Date
            // 
            this.day7Date.AutoSize = true;
            this.day7Date.Location = new System.Drawing.Point(17, 281);
            this.day7Date.Name = "day7Date";
            this.day7Date.Size = new System.Drawing.Size(35, 13);
            this.day7Date.TabIndex = 14;
            this.day7Date.Text = "label5";
            // 
            // day6Date
            // 
            this.day6Date.AutoSize = true;
            this.day6Date.Location = new System.Drawing.Point(17, 251);
            this.day6Date.Name = "day6Date";
            this.day6Date.Size = new System.Drawing.Size(35, 13);
            this.day6Date.TabIndex = 16;
            this.day6Date.Text = "label5";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(125, 95);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(30, 13);
            this.linkLabel1.TabIndex = 17;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "View";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(125, 128);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(30, 13);
            this.linkLabel2.TabIndex = 18;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "View";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // linkLabel3
            // 
            this.linkLabel3.AutoSize = true;
            this.linkLabel3.Location = new System.Drawing.Point(125, 160);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(30, 13);
            this.linkLabel3.TabIndex = 19;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "View";
            // 
            // linkLabel4
            // 
            this.linkLabel4.AutoSize = true;
            this.linkLabel4.Location = new System.Drawing.Point(125, 189);
            this.linkLabel4.Name = "linkLabel4";
            this.linkLabel4.Size = new System.Drawing.Size(30, 13);
            this.linkLabel4.TabIndex = 20;
            this.linkLabel4.TabStop = true;
            this.linkLabel4.Text = "View";
            // 
            // linkLabel5
            // 
            this.linkLabel5.AutoSize = true;
            this.linkLabel5.Location = new System.Drawing.Point(125, 221);
            this.linkLabel5.Name = "linkLabel5";
            this.linkLabel5.Size = new System.Drawing.Size(30, 13);
            this.linkLabel5.TabIndex = 21;
            this.linkLabel5.TabStop = true;
            this.linkLabel5.Text = "View";
            // 
            // linkLabel6
            // 
            this.linkLabel6.AutoSize = true;
            this.linkLabel6.Location = new System.Drawing.Point(125, 251);
            this.linkLabel6.Name = "linkLabel6";
            this.linkLabel6.Size = new System.Drawing.Size(30, 13);
            this.linkLabel6.TabIndex = 22;
            this.linkLabel6.TabStop = true;
            this.linkLabel6.Text = "View";
            // 
            // linkLabel7
            // 
            this.linkLabel7.AutoSize = true;
            this.linkLabel7.Location = new System.Drawing.Point(125, 281);
            this.linkLabel7.Name = "linkLabel7";
            this.linkLabel7.Size = new System.Drawing.Size(30, 13);
            this.linkLabel7.TabIndex = 23;
            this.linkLabel7.TabStop = true;
            this.linkLabel7.Text = "View";
            // 
            // tblLoggedDaysTableAdapter
            // 
            this.tblLoggedDaysTableAdapter.ClearBeforeFill = true;
            // 
            // Overview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(621, 414);
            this.Controls.Add(this.linkLabel7);
            this.Controls.Add(this.linkLabel6);
            this.Controls.Add(this.linkLabel5);
            this.Controls.Add(this.linkLabel4);
            this.Controls.Add(this.linkLabel3);
            this.Controls.Add(this.linkLabel2);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.day6Date);
            this.Controls.Add(this.day7Date);
            this.Controls.Add(this.day4Date);
            this.Controls.Add(this.day5Date);
            this.Controls.Add(this.day3Date);
            this.Controls.Add(this.day2Date);
            this.Controls.Add(this.day1Date);
            this.Controls.Add(this.mainChart);
            this.Controls.Add(this.label1);
            this.DataBindings.Add(new System.Windows.Forms.Binding("Tag", this.tblLoggedDaysBindingSource, "added", true));
            this.Name = "Overview";
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.mainChart, 0);
            this.Controls.SetChildIndex(this.day1Date, 0);
            this.Controls.SetChildIndex(this.day2Date, 0);
            this.Controls.SetChildIndex(this.day3Date, 0);
            this.Controls.SetChildIndex(this.day5Date, 0);
            this.Controls.SetChildIndex(this.day4Date, 0);
            this.Controls.SetChildIndex(this.day7Date, 0);
            this.Controls.SetChildIndex(this.day6Date, 0);
            this.Controls.SetChildIndex(this.linkLabel1, 0);
            this.Controls.SetChildIndex(this.linkLabel2, 0);
            this.Controls.SetChildIndex(this.linkLabel3, 0);
            this.Controls.SetChildIndex(this.linkLabel4, 0);
            this.Controls.SetChildIndex(this.linkLabel5, 0);
            this.Controls.SetChildIndex(this.linkLabel6, 0);
            this.Controls.SetChildIndex(this.linkLabel7, 0);
            ((System.ComponentModel.ISupportInitialize)(this.mainChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblLoggedDaysBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.myDatabaseDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataVisualization.Charting.Chart mainChart;
        private System.Windows.Forms.Label day1Date;
        private System.Windows.Forms.Label day2Date;
        private System.Windows.Forms.Label day3Date;
        private System.Windows.Forms.Label day5Date;
        private System.Windows.Forms.Label day4Date;
        private System.Windows.Forms.Label day7Date;
        private System.Windows.Forms.Label day6Date;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private System.Windows.Forms.LinkLabel linkLabel4;
        private System.Windows.Forms.LinkLabel linkLabel5;
        private System.Windows.Forms.LinkLabel linkLabel6;
        private System.Windows.Forms.LinkLabel linkLabel7;
        private MyDatabaseDataSet myDatabaseDataSet;
        private System.Windows.Forms.BindingSource tblLoggedDaysBindingSource;
        private MyDatabaseDataSetTableAdapters.tblLoggedDaysTableAdapter tblLoggedDaysTableAdapter;
    }
}