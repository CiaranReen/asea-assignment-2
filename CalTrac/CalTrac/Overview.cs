﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.Data.SqlClient;

namespace CalTrac
{
    public partial class Overview : CalTrac.BaseForm
    {
        SqlCeConnection conn = new SqlCeConnection("Data Source=C:\\temp\\MyDatabase.sdf");

        public Overview()
        {
            InitializeComponent();
            this.index();
        }

        public void index()
        {
            //Get the last 7 logged days
            string lastWeek = @"SELECT TOP 7 added FROM tblLoggedDays WHERE userId = 1 ORDER BY added DESC";

            using (SqlCeDataAdapter da = new SqlCeDataAdapter(lastWeek, conn))
            {
                conn.Open();
                DataTable ds = new DataTable();
                da.Fill(ds);

                //Fill each text box components with the appropriate date
                day1Date.Text = DateTime.Parse(ds.Rows[0][0].ToString()).ToString("dd-MM-yyyy");
                day2Date.Text = DateTime.Parse(ds.Rows[1][0].ToString()).ToString("dd-MM-yyyy");
                day3Date.Text = DateTime.Parse(ds.Rows[2][0].ToString()).ToString("dd-MM-yyyy");
                day4Date.Text = DateTime.Parse(ds.Rows[3][0].ToString()).ToString("dd-MM-yyyy");
                day5Date.Text = DateTime.Parse(ds.Rows[4][0].ToString()).ToString("dd-MM-yyyy");
                day6Date.Text = DateTime.Parse(ds.Rows[5][0].ToString()).ToString("dd-MM-yyyy");
                day7Date.Text = DateTime.Parse(ds.Rows[6][0].ToString()).ToString("dd-MM-yyyy");
                conn.Close();
            }
        }

        private void dayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //DayTracker dt = new DayTracker();
            this.Hide();
            //dt.ShowDialog();           
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string queryText = @"SELECT * FROM tblLoggedDays WHERE userId = 1 AND id = 1";
            using (SqlCeDataAdapter da = new SqlCeDataAdapter(queryText, conn))
            {
                conn.Open();
                DataTable ds = new DataTable();
                da.Fill(ds);
                MessageBox.Show(ds.Rows[0][0].ToString());
                conn.Close();
            }
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string queryText = @"SELECT * FROM tblLoggedDays WHERE userId = 1 AND id = 2";
            using (SqlCeDataAdapter da = new SqlCeDataAdapter(queryText, conn))
            {
                conn.Open();
                DataTable ds = new DataTable();
                da.Fill(ds);
                MessageBox.Show(ds.Rows[0][0].ToString());
                conn.Close();
            }
        }
    }
}
